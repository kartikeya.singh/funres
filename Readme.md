# **FunRes** 

## FunRes is a computational method designed for the identification of functional cell states. FunRes utilises tissue single cell RNA-seq data to reconstruct the functional cell-cell communication network which is leveraged for partitioning each cell type into functional states.


#### Authors
FunRes was developed in the [Computational Biology Group](https://wwwen.uni.lu/lcsb/research/computational_biology) by

- [Kartikeya Singh](https://wwwen.uni.lu/lcsb/people/kartikeya_singh)
- [Sascha Jung](https://www.cicbiogune.es/people/sjung)
- [Antonio del Sol](https://wwwfr.uni.lu/lcsb/people/antonio_del_sol_mesa)